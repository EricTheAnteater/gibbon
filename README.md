# gibbon

## Introduction
Gibbon is an AI-powered oracle informed by the text of _The History of the Decline and Fall of the Roman Empire_.

## Technical Notes
Gibbon uses OpenAI's ChatGPT and Embeddings functionalities. The basic prompt functionality is accomplished with
a simple call to OpenAI using the ChatCompletion.create method. What makes things a little more interesting than
just that is that relevant text from the aforementioned classic is given to the AI whenever a question is asked.
The text and the relevance of each section vis-a-vis the question is obtained by downloading the text from a
public source, caching it, then building an in-RAM vector table using Pandas out of it by chunking the text (by
paragraph) and using OpenAI's embeddings endpoint for each paragraph. When a query is made, an embedding is
generated for that query, and that embedding is fed to a simple function which returns the closes embeddings
found in the table. The text associated with the closest embeddings is then inserted into the text sent to
OpenAI, along with the text of the query, and a response is returned.

The main file, gibbon.py, can be used either as a standalone script or as a module for use in some larger application.

#### Example usage
##### Script
```
python gibbon.py "What's up with Pertinax?"
```
##### As a Module
```
from .gibbon import main
answer = main("What's up with Pertinax?")
print(answer)
```

## Notes
This project was inspired by some content found in the OpenAI Cookbook at https://github.com/openai/openai-cookbook.

The full text of _Decline and Fall_ can be found here: https://standardebooks.org/ebooks/edward-gibbon/the-history-of-the-decline-and-fall-of-the-roman-empire