import openai
import os
import requests
from dotenv import load_dotenv
from bs4 import BeautifulSoup
import pandas as pd
from scipy import spatial
from os.path import exists
import json
import tiktoken
import sys

load_dotenv()

VERBOSE = False

EMBEDDING_MODEL = "text-embedding-ada-002"
CHAT_MODEL = "gpt-4"

TEXT_LOCATION = "https://standardebooks.org/ebooks/edward-gibbon/the-history-of-the-decline-and-fall-of-the-roman-empire/text/single-page"
CACHE_FN = "cache.cache"
EMBED_FN = "embed.cache"
MIN_EMBED_INPUT_LENGTH = 16
MAX_EMBED_TOKEN_COUNT = 8191
PROMPT_PIECE_LENGTH_LIMIT = 2048

START_OF_BOOK = "extent and military force of the empire in the age of the Antonines"
END_OF_BOOK = "Lausanne, June 27, 1787"

GLOBAL_EMBEDS = None

def print_to_screen(text):
    if VERBOSE:
        print(text);

def get_embed_for_text(text):
    embedding = openai.Embedding.create(
        input=text, model=EMBEDDING_MODEL
    )["data"][0]["embedding"]

    return embedding

def find_chunk_index(chunks, looking_for):
    # Return the chunk index which first meets the looking_for query. Useful for finding the start and end of the book.
    for i, chunk in enumerate(chunks):
        if looking_for in chunk:
            return i

def get_embeds_from_file():
    with open(EMBED_FN, "r", encoding="utf-8") as embed_file:
        embeds = json.loads(embed_file.read())
        return embeds;

def ingest():
    print_to_screen("Handling embeddings.")
    text = ""
    embeds = []
    encoding = tiktoken.encoding_for_model(EMBEDDING_MODEL)


    # Get the data and build out a table of text->embed, caching both embeds and test.
    if not exists(EMBED_FN):
        if not exists(CACHE_FN):
            print_to_screen("Downloading file...")
            html = requests.get(TEXT_LOCATION).text
            soup = BeautifulSoup(html, 'html.parser')
            text = soup.get_text()
            with open(CACHE_FN, "w", encoding="utf-8") as cache_file:
                cache_file.write(text)
        else:
            print_to_screen("Loading from cache...")
            with open(CACHE_FN, "r", encoding="utf-8") as cache_file:
                text = cache_file.read()

        chunks = text.split('.\n')
        print_to_screen("Total number of chunks in document: {}.".format(len(chunks)))
        chunk_index_start_of_book = find_chunk_index(chunks, START_OF_BOOK)
        print_to_screen("Start chunk: {}.".format(chunk_index_start_of_book))
        chunk_index_end_of_book = find_chunk_index(chunks, END_OF_BOOK)
        print_to_screen("End chunk: {}.".format(chunk_index_end_of_book))
        chunks = chunks[chunk_index_start_of_book:chunk_index_end_of_book]
        print_to_screen("Total number of chunks to potentially create embeddings: {}.".format(len(chunks)))

        for i, chunk in enumerate(chunks):
            # Strip out whitespace or tiny fragments.
            if len(chunk) > MIN_EMBED_INPUT_LENGTH and not chunk.isspace():
                # Generate the embed
                if len(encoding.encode(chunk)) > MAX_EMBED_TOKEN_COUNT:
                    print_to_screen("Truncating large chunk.")
                    while len(encoding.encode(chunk)) > MAX_EMBED_TOKEN_COUNT:
                        chunk = chunk[:-1]
                print_to_screen("Creating embedding for chunk {} of {}".format(i, len(chunks)))
                embed = get_embed_for_text(chunk)
                embeds.append({"text": chunk, "embedding": embed})

            else:
                print_to_screen("Skipping chunk-- too small.")

        with open(EMBED_FN, "w", encoding="utf-8") as embed_file:
                embed_file.write(json.dumps(embeds))
    else:
        embeds = get_embeds_from_file()

    # Convert the table into something usable for our purposes.
    embeds = pd.DataFrame(embeds)
    return embeds

def init():
    # Set the key and print_to_screen debug info.
    openai.api_key = os.getenv("OPENAI_API_KEY")
    print_to_screen("gibbon.ai\n\n\n")


def get_top_results_from_query(embeds, query_text, top_count):
    # Basic relatedness search
    query_embed = get_embed_for_text(query_text)
    relatedness_function = lambda x, y: 1 - spatial.distance.cosine(x, y)
    strings_by_relatedness = [
        {"text": row["text"], "relatedness": relatedness_function(query_embed, row["embedding"])}
        for i, row in embeds.iterrows()
    ]
    strings_by_relatedness.sort(key=lambda x: x["relatedness"], reverse=True)
    top_strings = [s["text"] for s in strings_by_relatedness]
    return top_strings[:top_count]

def ingest_if_needed():
    #Memoize
    global GLOBAL_EMBEDS
    if GLOBAL_EMBEDS is None:
        GLOBAL_EMBEDS = ingest()

def ask_question(user_input):

    if user_input:
        top_results = get_top_results_from_query(GLOBAL_EMBEDS, user_input, 3)
        response = get_response_with_context(user_input, top_results)
        return(response)
    else:
        return("No input given.")


def get_response_with_context(prmpt, context):
    messages = [
        {"role": "system", "content": "You are Gibbon, expert on the Roman Empire's history, decline, and fall."},
        {"role": "user", "content": "Hi! Who are you?"},
        {"role": "assistant", "content": "I am Gibbon."},
        {"role": "user", "content": "Tell me about Rome's happy times."},
        {"role": "assistant", "content": "In the second century of the Christian Era, the empire of Rome comprehended the fairest part of the earth, and the most civilized portion of mankind. The frontiers of that extensive monarchy were guarded by ancient renown and disciplined valor. The gentle but powerful influence of laws and manners had gradually cemented the union of the provinces. Their peaceful inhabitants enjoyed and abused the advantages of wealth and luxury. The image of a free constitution was preserved with decent reverence: the Roman senate appeared to possess the sovereign authority, and devolved on the emperors all the executive powers of government. During a happy period of more than fourscore years, the public administration was conducted by the virtue and abilities of Nerva, Trajan, Hadrian, and the two Antonines."},
        {"role": "user", "content": "What is an example of something bad that happened in Rome?"},
        {"role": "assistant", "content": "Within less than eighteen months, two unexpected revolutions overturned the ambitious schemes of Galerius. The hopes of uniting the western provinces to his empire were disappointed by the elevation of Constantine; whilst Italy and Africa were lost by the successful revolt of Maxentius."},
    ]

    truncated_context = [x[:PROMPT_PIECE_LENGTH_LIMIT] for x in context]
    extra_context = '. '.join(truncated_context)

    text_content = ("I am going to give you some information about the decline and fall of Rome. "
                   "I will then say something to you. If the information is relevant to the topic of what I say, "
                   "use the information in your reply. If it is not, use the tone, vocabulary, and style of the information "
                   "while discussing what I say. Do not tell me whether the information is relevant to the topic."
                   "Here is the information: {}. My topic of discussion is this: {}.").format(extra_context, prmpt[:PROMPT_PIECE_LENGTH_LIMIT])
    messages.append({"role": "user", "content": text_content})
    completion = openai.ChatCompletion.create(model=CHAT_MODEL, messages=messages)
    return completion["choices"][0]["message"]["content"]
    #return extra_context
    #return messages



if __name__ == "__main__":
    init()
    ingest_if_needed()
    print(ask_question(sys.argv[1] if len(sys.argv) > 1 else ""))
